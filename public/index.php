<?php

use Core\Creational\Builder\Conceptual\Request\BuilderRequest;
use Core\Creational\Builder\Conceptual\Request\MethodsEnum;
use Core\Creational\Singleton\Conceptual\Singleton;
use Core\Creational\Singleton\Practical\DbConnection;

require_once "../vendor/autoload.php";

//$request = new BuilderRequest;
//$request->url('http://localhost/api/v1/users');
//$request->method(MethodsEnum::GET);
//$request->payload(['filters' => 'abc']);
//$request->build();
//
//$request = (new BuilderRequest)
//        ->url('http://localhost/api/v1/users')
//        ->method(MethodsEnum::GET)
//        ->build();
//
//var_dump($request);

/**
 * Singleton Conceptual
 */

//$instanceA = Singleton::getInstance();
//$instanceB = Singleton::getInstance();
//
//var_dump($instanceA === $instanceB);

/**
 * Singleton Practical
 */

//$instance = DbConnection::getInstance();
//DbConnection::getInstance();
//DbConnection::getInstance();
//DbConnection::getInstance();
//DbConnection::getInstance();
//DbConnection::getInstance();

DbConnection::getConnection();
DbConnection::getConnection();
DbConnection::getConnection();
DbConnection::getConnection();
DbConnection::getConnection();